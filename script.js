/*
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
   Екранування позначається символом / та існує щоб інтерпретатор не зчитував наступний символ після екранування як один із символів рядка (текст).
   Так, в JS рядки позначаються лапками (одинарними, подвійними чи зворотніми). Все що знаходиться між двома однаковими символами лапок
   інтерпретується як рядок. Якщо посеред рядка необхідно використати такі ж лапки, їх необхідно екранувати, наприклад "Some string with /"quotes/"."
   Також із знака / починаються спецсимволи, як символ наступного ряда /n чи табуляції /t.

2. Які засоби оголошення функцій ви знаєте?
  З допомогою ключового слова function, тобто Function Declaration.
  Присвоєння функції змінній, тобто Function Expression.
  Стрілкові функції.

3. Що таке hoisting, як він працює для змінних та функцій?
  hoisting це підняття. Це означає що під час парсингу коду оголошення функцій (через Function Declaration) "піднімається в гору".
  Таким чином, виклик функції, оголошеної через ключове слова function, можливий до цього оголошення. Але це не працює зі стрілочними
  функціями та Function Expression. 
  Щодо змінних, то піднимається лише оголошення змінної, але не її значення. Якщо звернутися до змінної раніше, ніж вона оголошенна із значенням,
  то її значення буде undefined. 
*/

"use strict";

// Написати функцію, яка буде створювати та повертати об'єкт.
function createNewUser() {
  let newUser = {
    firstName: "",
    lastName: "",
    birthday: "",

    // Додати метод, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі
    getLogin() {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
    // Створити метод, який повертатиме скільки користувачеві років.
    getAge() {
      let nowDate = Date.now();
      let userBirthday = this.birthday.slice(-4) + "-" + this.birthday.slice(3, 5) + "-" + this.birthday.slice(0, 2);
      let userBirthdayDate = Date.parse(userBirthday);
      let userAge = Math.trunc((nowDate - userBirthdayDate) / 1000 / 60 / 60 / 24 / 365.25);
      return userAge;
    },
    // Створити метод, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження.
    getPassword() {
      return (this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + this.birthday.slice(-4));
    },

    // Створити функції-сеттери, які дозволять змінити властивості
    setFirstName(newFirstName) {
      return Object.defineProperty(newUser, "firstName", {
        value: newFirstName,
      });
    },
    setLastName(newLastName) {
      return Object.defineProperty(newUser, "firstName", {
        value: newLastName,
      });
    },
  };
  // При виклику функція повинна запитати ім'я та прізвище.
  Object.defineProperty(newUser, "firstName", {
    value: prompt("Enter your name"),
    writable: false,
    configurable: true,
  });
  Object.defineProperty(newUser, "lastName", {
    value: prompt("Enter your surname"),
    writable: false,
    configurable: true,
  });
  // При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy).
  Object.defineProperty(newUser, "birthday", {
    value: prompt("Enter your birthday", "dd.mm.yyyy"),
  });

  return newUser;
}

const myUser = createNewUser();

const myUserLogin = myUser.getLogin();
const myUserPassword = myUser.getPassword();
const myUserAge = myUser.getAge();

console.log(myUser);
console.log(myUserLogin);
console.log(myUserPassword);
console.log(myUserAge);
